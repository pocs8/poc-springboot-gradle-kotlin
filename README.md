# POC - CRUD SPRING BOOT - KOTLIN
## Prova de conceito - CRUD SPRING BOOT - KOTLIN

> O projeto tem como objetivo demonstrar a ultilização do Kotlin com SpringBoot, com um crud de Pessoa.

# Tecnologias
 - Kotlin 1.3.718
 - Spring Boot 2.2.7.RELEASE
     - spring-boot-starter-web
     - spring-boot-starter-data-jpa
     - spring-boot-devtools
 - Flywaydb
 - H2
 - Tomcat (Embedded no Spring Boot)
 - Git (GitLab)
 
 # Execução
 
 A execução das aplicações são feitas através do de um comando Gradle que envoca a inicialização do Spring Boot.
 
 - Scripts
     - ```./gradlew bootRun```

