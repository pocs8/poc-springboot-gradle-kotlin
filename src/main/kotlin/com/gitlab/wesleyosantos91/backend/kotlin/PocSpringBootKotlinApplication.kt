package com.gitlab.wesleyosantos91.backend.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PocSpringBootKotlinApplication

fun main(args: Array<String>) {
    runApplication<PocSpringBootKotlinApplication>(*args)
}
