package com.gitlab.wesleyosantos91.backend.kotlin.exception

/**
 *
 * @author : wesleyosantos91
 * @Date : 08/05/20
 * @Contact : wesleyosantos91@gmail.com
 *
 **/
class ObjectNotFoundException(message:String): Exception(message)